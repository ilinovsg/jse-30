package ru.ilinovsg.tm.service;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class ProjectServiceTest {

    @Mock
    private ProjectRepository projectRepository = Mockito.mock(ProjectRepository.class);;

    private ProjectService projectService;

    private static Project project = new Project("PROJECT 3", "test");

    @BeforeEach
    void setUp() {
        projectService = ProjectService.getInstance();
        projectService.create("PROJECT 3", "test");
    }

    @Test
    void create(){
        assertEquals(-1, projectService.create(null, null));
        assertEquals(-1, projectService.create("", ""));
        assertEquals(0, projectService.create("project1", "test"));
        assertNotNull(projectService.create("PR3", "TEST"));
    }

    @Test
    void update()  {
        assertNull(projectService.update(null, null, null));
        assertNull(projectService.update(1L, null, "test"));
        assertNull(projectService.update(1L, "", "test"));
        assertNull(projectService.update(1L, "PR1", null));
        assertNull(projectService.update(1L, "PR1", ""));
        assertNull(projectService.update(1L, "PR", "test"));
    }

    @Test
    void findByIndex() {
        assertNull(projectService.findByIndex(-100));
        assertNull(projectService.findByIndex(100));
        assertNotNull(projectService.findByIndex(0));
    }

    @Test
    void removeByIndex() {
        assertNull(projectService.removeByIndex(-100));
        assertNull(projectService.removeByIndex(100));
        assertNotNull(projectService.removeByIndex(0));
    }

    @Test
    void findByName() {
        assertNull(projectService.findByName(null));
        assertNull(projectService.findByName(""));
        assertNotNull(projectService.findByName("PROJECT 3"));
    }

    @Test
    void removeByName() {
        assertNull(projectService.removeByName(null));
        assertNull(projectService.removeByName(""));
        assertNotNull(projectService.removeByName("PROJECT 3"));
    }

    @Test
    void findById() throws ProjectNotFoundException {
        assertNull(projectService.findById(null));
        assertThrows(ProjectNotFoundException.class,() -> projectService.findById(1L));
        Long id = projectService.findByName("PROJECT 3").getId();
        assertNotNull(projectService.findById(id));
    }

    @Test
    void removeById() {
        assertNull(projectService.removeById(null));
        Long id = projectService.findByName("PROJECT 3").getId();
        assertNotNull(projectService.removeById(id));
    }

    @Test
    void findAll() {
        assertNotNull(projectService.findAll());
    }

    @Test
    void findAllByUserId() {
        assertNull(projectService.findAllByUserId(null));
        assertNotNull(projectService.findAllByUserId(1L));
    }
}